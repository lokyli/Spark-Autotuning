package fetcher;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.sql.Time;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import org.json.JSONException;
import others.DataType;


/*
 * 一条运行记录
 * fields： appid 运行时间 运行参数 运行结果 运行数据（抽象特征）
 * functions :读取运行记录，展示运行记录
 * 
 * 
 * */
public class RunRecord {
	String applicationID,masterMode,applicationName =null; //appid
	public SparkConfiguration conf =null; //运行参数
	long applicationStartTime;
	long applicationEndTime ;
	public String getApplicationID() {
		return applicationID;
	}

	public long getApplicationEndTime() {
		return applicationEndTime;
	}

	public void setApplicationEndTime(long applicationEndTime) {
		this.applicationEndTime = applicationEndTime;
	}

	public void setApplicationID(String applicationID) {
		this.applicationID = applicationID;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public long getApplicationStartTime() {
		return applicationStartTime;
	}

	public void setApplicationStartTime(long applicationStartTime) {
		this.applicationStartTime = applicationStartTime;
	}

	public ArrayList<StageInfo> stageList= null;

	
	//给API使用的
	public RunRecord(){
		
	}
		 
	 
	 //为了降低python的难度，这个函数用于解析 conf中的字符串为 MB形式，并且直接只输出数字
	 public int parseConfIntoMB(String conf){ 
		 String tmp;
		 tmp=conf.substring(0, conf.length()-1);//去尾 nNmM 正则表达式由于使用字符串·所以还是要使用两个反斜杠
		 if(conf.matches("^\\d+[mM]")){
			 return Integer.parseInt(tmp);
		 }
		 else if(conf.matches("^\\d+[gG]")){
			 return 1024*Integer.parseInt(tmp);
		}
		else return -1; //解析出错
	 }
	 
	 //这个函数用于解析 conf中的字符串为 boolean形式   TRUE　FLASE true false
	 public boolean parseConfIntoBoolean(String conf){
		 String tmp =conf.toLowerCase();
		 if(tmp.equals("true")) return true;
		 else return false;
	 }
	 

}

