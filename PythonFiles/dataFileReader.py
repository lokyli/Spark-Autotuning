#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@Time    : 2019/3/27 10:26
@Author  : tonychao
@File    : dataFileReader.py
功能：
"""

'''
此函数用于读取建立机器学习模型所需的数据
'''

def readFile (filePath):
    #filePath="./historyDataWhenDBnotAvaliable/Application_"+app_name+"_"+app_md5+"_"+app_class+".data"
    f=open(filePath,'r')
    count_of_groups=eval(f.readline())
    records=f.read().split("#\n")

    groupConfList = [[] * 1 for i in range(count_of_groups)]
    totalTimeList=[]
    groupTimeList = [[] * 1 for i in range(count_of_groups)]

    for record in records:
        if record=="":
            continue
        #print record
        lines=record.splitlines()
        totalTime=long(lines[0].split()[1])
        totalTimeList.append(totalTime)
        app_id=lines[0].split()[0]
        #index1=组号，index2=记录号
        for i in range(1,count_of_groups+1):
            tmp=lines[i].split()
            groupTimeList[i-1].append(long(tmp.pop(0)))
            tmp2 = []
            for conf in tmp:
                tmp2.append(eval(conf))
            groupConfList[i-1].append(tmp2)
    f.close()
    return [count_of_groups,totalTimeList,groupConfList,groupTimeList]

#readFile("name","md5","class")

